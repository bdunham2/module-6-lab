#!/bin/bash
# code that takes in any file or string inputted by the user
# intended to input the Bee Movie script and replace keywords with Star Wars terms

word1="bee"
replace1="jedi"
word2="Barry"
replace2="Anakin"
word3="Adam"
replace3="Obi-Wan"
word4="Vanessa"
replace4="Padme"
x="true"

while [[ $x == "true" ]]
do 
 if [[ $word1 != $replace1 ]]
 then sed -i 's/bee/jedi/;s/wings/starfighters/' $@
 fi
 if [[ $word2 != $replace2 ]]
 then sed -i 's/Barry/Anakin/;s/Benson/Skywalker/' $@
 fi
 if [[ $word3 != $replace3 ]]
 then sed -i 's/Adam/Obi-Wan/;s/Flayman/Kenobi/' $@
 fi
 if [[ $word4 != $replace4 ]]
 then sed -i 's/Vanessa/Padme/;s/Bloome/Amidala/' $@
 fi
 x="false"
done
